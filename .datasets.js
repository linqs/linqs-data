"use strict";
window.linqs = window.linqs || {};
window.linqs.data = window.linqs.data || {};

window.linqs.data.datasets = {
    "arxiv": {
        "citation": "@inproceedings{nr,title={Collective entity resolution in relational data},author={Bhattacharya, Indrajit and Getoor, Lise},booktitle={TKDD},year={2007}}",
        "description": "<p>The arXiv dataset describes high energy physics publications. It was originally used in KDD Cup 2003 . It contains 29555 papers with 58515 references to 9200 authors. The attribute information available for this dataset is also just the author name, with the same variations in form as described above.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/arxiv/arxiv.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "arxiv",
        "md5": "4c720cfeaebdbc384943a8fbdadf792e",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2007/bhattacharya:tkdd07/bhattacharya-tkdd.pdf",
                "text": "Indrajit Bhattacharya, and Lise Getoor. \"Collective entity resolution in relational data.\" TKDD. 2007."
            }
        ],
        "size": 1189,
        "title": "ArXiv"
    },
    "citeseer-doc-classification": {
        "citation": "@inproceedings{nr,title={The Network Data Repository with Interactive Graph Analytics and Visualization},author={Ryan A. Rossi and Nesreen K. Ahmed},booktitle={AAAI},url={http://networkrepository.com},year={2015}}",
        "description": "<p>The CiteSeer dataset consists of 3312 scientific publications classified into one of six classes. The citation network consists of 4732 links. Each publication in the dataset is described by a 0/1-valued word vector indicating the absence/presence of the corresponding word from the dictionary. The dictionary consists of 3703 unique words. The README file in the dataset provides more details.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/citeseer-doc-classification/citeseer-doc-classification.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "citeseer-doc-classification",
        "md5": "c16c09fac412b868abc6658a73a07684",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2003/lu:icml03/",
                "text": "Qing Lu, and Lise Getoor. \"Link-based classification.\" ICML, 2003."
            },
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2008/sen:aimag08/",
                "text": "Prithviraj Sen, et al. \"Collective classification in network data.\" AI Magazine, 2008."
            }
        ],
        "size": 376,
        "title": "CiteSeer for Document Classification"
    },
    "citeseer-entity-resolution": {
        "citation": "@inproceedings{nr,title={The Network Data Repository with Interactive Graph Analytics and Visualization},author={Ryan A. Rossi and Nesreen K. Ahmed},booktitle={AAAI},url={http://networkrepository.com},year={2015}}",
        "description": "<p>The CiteSeer dataset contains 1504 machine learning documents with 2892 author references to 165 author entities. For this dataset, the only attribute information available is author name. The full last name is always given, and in some cases the author’s full first name and middle name are given and other times only the initials are given.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/citeseer-entity-resolution/citeseer-entity-resolution.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "citeseer-entity-resolution",
        "md5": "93148fe10eeb408721c86cf0228ef42f",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2007/bhattacharya:tkdd07/bhattacharya-tkdd.pdf",
                "text": "Indrajit Bhattacharya, and Lise Getoor. \"Collective entity resolution in relational data.\" TKDD. 2007."
            }
        ],
        "size": 92,
        "title": "CiteSeer for Entity Resolution"
    },
    "cora": {
        "citation": "@inproceedings{nr,title={Collective classification in network data},author={Sen, Prithviraj},booktitle={AI Magazine},url={https://relational.fit.cvut.cz/dataset/CORA},year={2015}}",
        "description": "<p>The Cora dataset consists of 2708 scientific publications classified into one of seven classes. The citation network consists of 5429 links. Each publication in the dataset is described by a 0/1-valued word vector indicating the absence/presence of the corresponding word from the dictionary. The dictionary consists of 1433 unique words. The README file in the dataset provides more details.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/cora/cora.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "cora",
        "md5": "b8b86a8e844eef1ef0ee1ff244f8f6d9",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2003/lu:icml03/",
                "text": "Qing Lu, and Lise Getoor. \"Link-based classification.\" ICML, 2003."
            },
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2008/sen:aimag08/",
                "text": "Prithviraj Sen, et al. \"Collective classification in network data.\" AI Magazine, 2008."
            }
        ],
        "size": 193,
        "title": "Cora"
    },
    "drug-target-interaction": {
        "citation": "@article{fakharei2014network, title={Network-Based Drug-Target Interaction Prediction with Probabilistic Soft Logic}, author={Fakhraei, Shobeir and Huang, Bert and Raschid, Louiqa and Getoor, Lise}, journal={IEEE/ACM Transactions on Computational Biology and Bioinformatics}, year={2014},}",
        "description": "<p>This dataset contains interactions between drugs and targets collected from DrugBank, KEGG Drug, DCDB, and Matador. It was originally collected by <a href=\"https://www.liebertpub.com/doi/abs/10.1089/cmb.2010.0213\">Perlman et al</a>. It contains 315 drugs, 250 targets, 1,306 drug-target interactions, 5 types of drug-drug similarities, and 3 types of target-target similarities. Drug-drug similarities include Chemical-based, Ligand-based, Expression-based, Side-effect-based, and Annotation-based similarities. Target-target similarities include Sequence-based, Protein-protein interaction network-based, and Gene Ontology-based similarities. The original task on the dataset is to predict new interactions between drugs and targets based on different types of similarities in the network.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/drug-target-interaction/drug-target-interaction.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "drug-target-interaction",
        "md5": "b9ba7709bb49768f67645b31feccc0ed",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2014/fakhraei:tcbb14/",
                "text": "Shobeir Fakhraei, et al. \"Network-based drug-target interaction prediction with probabilistic soft logic.\" IEEE/ACM TCBB, 2014."
            },
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2016/fakhraei:mlg16/",
                "text": "Shobeir Fakhraei, et al. \"Adaptive Neighborhood Graph Construction for Inference in Multi-Relational Networks.\" MLG, 2016."
            }
        ],
        "size": 1477025,
        "title": "Drug-Target Interaction"
    },
    "pubmed-diabetes": {
        "citation": "@inproceedings{nr,title={Query-driven Active Surveying for Collective Classification},author={Namata, Galileo et al.},booktitle={MLG},year={2012}}",
        "description": "<p>The Pubmed Diabetes dataset consists of 19717 scientific publications from PubMed database pertaining to diabetes classified into one of three classes. The citation network consists of 44338 links. Each publication in the dataset is described by a TF/IDF weighted word vector from a dictionary which consists of 500 unique words. The README file in the dataset provides more details.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/pubmed-diabetes/pubmed-diabetes.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "pubmed-diabetes",
        "md5": "caa97c29fda75f3145e350f600963365",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2012/namata:mlg12-wkshp/namata-mlg12.pdf",
                "text": "Galileo Namata, et. al. \"Query-driven Active Surveying for Collective Classification.\" MLG. 2012."
            }
        ],
        "size": 14407,
        "title": "PubMed Diabetes"
    },
    "social-spammers": {
        "citation": "@inproceedings{fakhraei2015collective, author = {Fakhraei, Shobeir and Foulds, James and Shashanka, Madhusudana and Getoor, Lise}, title = {Collective Spammer Detection in Evolving Multi-Relational Social Networks},booktitle = {Proceedings of the 21th ACM SIGKDD International Conference on Knowledge Discovery and Data Mining},series = {KDD '15},year = {2015},isbn = {978-1-4503-3664-2},location = {Sydney, NSW, Australia},pages = {1769--1778},doi = {10.1145/2783258.2788606},publisher = {ACM},}",
        "description": "<p>This anonymized dataset was collected from the Tagged.com social network website. It contains 5.6 million users and 858 million links between them. Each user has 4 features and is manually labeled as \"spammer\" or \"not spammer\". Each link represents an action between two users and includes a timestamp and a type. The network contains 7 anonymized types of links. The original task on the dataset is to identify (i.e., classify) the spammer users based on their relational and non-relational features.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/social-spammer/social-spammer.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "social-spammers",
        "md5": "8afa6d1c8b0e6c7cdb11331098c0b5c7",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2015/fakhraei:kdd15/",
                "text": "Shobeir Fakhraei, et al. \"Collective spammer detection in evolving multi-relational social networks.\" KDD, 2015."
            }
        ],
        "size": 3998585,
        "title": "Social Spammers"
    },
    "stance-classification": {
        "citation": "@inproceedings{abbott2016LREC, title={Internet Argument Corpus 2.0: An SQL schema for Dialogic Social Media and the Corpora to go with it}, author={Abbot,Rob and Ecker,Brian and Anand,Pranav and Walker, Marilyn}, booktitle={In Language Resources and Evaluation Conference (LREC)}, year={2016},publisher={European Language Resources Association},}",
        "description": "<p>This dataset contains threads containing short user posts on debate topics across multiple online forums. The well-studied forums are 4Forums.com (on average 340 users per topic and 19 posts per user) and CreateDebate.org (310 users per topic and 4 posts per user). The key task is classifying the users’ stances towards discussion topics and classifying the polarity of replies between users. 4Forums.com has crowd-sourced annotations with high inter-annotator agreement for stances of users in each topic and dis/agreement between users that reply to one another. CreateDebate.org supports self-labeling for stance and dis/agreement, but for each post authored by a user.</p>",
        "downloadLink": "https://nlds.soe.ucsc.edu/iac2",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "stance-classification",
        "md5": "n/a",
        "references": [
            {
                "link": "http://www.aclweb.org/anthology/W14-27#page=121",
                "text": "Dhanya Sridhar, Lise Getoor, Marilyn Walker. \"Collective stance classification of posts in online debate forums.\" In ACL Workshop on Social Dynamics and Personal Attributes in Social Media. 2014."
            },
            {
                "link": "http://www.aclweb.org/website/old_anthology/P/P15/P15-1012.pdf",
                "text": "Dhanya Sridhar, James Foulds, Bert Huang, Marilyn Walker, Lise Getoor. \"Joint Models of Disagreement and Stance in Online Debates.\" In ACL. 2015."
            }
        ],
        "size": 5164591,
        "title": "Stance Classification"
    },
    "terrorist-attacks": {
        "citation": "@inproceedings{zhouICML2006, title={Entity and Relationship Labeling in Affiliation Networks}, author={Zhou, Bin et al.}, booktitle={Proceedings of the Twentieth International Conference on Machine Learning}, year={2006}, publisher={ICML},}",
        "description": "<p>This dataset consists of 1293 terrorist attacks each assigned one of 6 labels indicating the type of the attack. Each attack is described by a 0/1-valued vector of attributes whose entries indicate the absence/presence of a feature. There are a total of 106 distinct features. The files in the dataset can be used to create two distinct graphs. The README file in the dataset provides more details.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/terrorist-attacks/terrorist-attacks.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "terrorist-attacks",
        "md5": "047fdd1a2dd5f480b623223dcb17ae6e",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2006/zhao:sna06/zhaosna06.pdf",
                "text": "Bin Zhao, et. al. \"Entity and Relationship Labeling in Affiliation Networks.\" ICML. 2006."
            }
        ],
        "size": 56,
        "title": "Terrorist Attacks"
    },
    "terrorists": {
        "citation": "@inproceedings{zhouICML2006, title={Entity and Relationship Labeling in Affiliation Networks}, author={Zhou, Bin et al.}, booktitle={Proceedings of the Twentieth International Conference on Machine Learning}, year={2006}, publisher={ICML},}",
        "description": "<p>This dataset contains information about terrorists and their relationships. This dataset was designed for classification experiments aimed at classifying the relationships among terrorists. The dataset contains 851 relationships, each described by a 0/1-valued vector of attributes where each entry indicates the absence/presence of a feature. There are a total of 1224 distinct features. Each relationship can be assigned one or more labels out of a maximum of four labels making this dataset suitable for multi-label classification tasks. The README file provides more details.</p>",
        "downloadLink": "https://linqs-data.soe.ucsc.edu/public/terrorists/terrorists.tar.gz",
        "img": "https://linqs.soe.ucsc.edu/sites/default/files/linqs_logo_1.png",
        "key": "terrorists",
        "md5": "d7001dc40c52688a06b5bbf11f808599",
        "references": [
            {
                "link": "https://linqspub.soe.ucsc.edu/basilic/web/Publications/2006/zhao:sna06/zhaosna06.pdf",
                "text": "Bin Zhao, et. al. \"Entity and Relationship Labeling in Affiliation Networks.\" ICML. 2006."
            }
        ],
        "size": 248,
        "title": "Terrorists"
    }
};