#!/usr/bin/env python3

import sys
from urllib.request import urlretrieve

def main():
    url = sys.argv[1]
    file = sys.argv[2]

    out = urlretrieve(url, filename = file)
    print(out[0])

if (__name__ == "__main__"):
    main()