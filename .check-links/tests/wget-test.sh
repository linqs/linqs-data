#!/bin/bash

function test () {
    local url=$1
    local out=$2

    wget -q $url -O $out
    echo $out
}

function main () {
    local url=$1
    local out=$2

    test $url $out
}

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@"