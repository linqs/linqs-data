#!/usr/bin/env python3

import sys
import wget

def main():
    url = sys.argv[1]
    file = sys.argv[2]

    out = wget.download(url, file)
    print(out)

if (__name__ == "__main__"):
    main()