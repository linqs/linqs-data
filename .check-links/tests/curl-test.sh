#!/bin/bash

function test () {
    local url=$1
    local out=$2

    curl -s -k $url -o $out
    echo $out
}

function main () {
    local url=$1
    local out=$2

    test $url $out
}

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@"