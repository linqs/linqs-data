#!/usr/bin/env python3

import sys

from chainer.dataset import cached_download

def main():
    url = sys.argv[1]
    file = sys.argv[2]

    out = cached_download(url)
    print(out)

if (__name__ == "__main__"):
    main()