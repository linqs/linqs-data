#!/bin/bash

LINKS=links.txt
OUT=file.txt

function log_error () {
    local script=$1
    local url=$2

    echo "Error: $2 in $1"
}

function log_success () {
    local script=$1
    local url=$2

    echo "Success: $2 in $1"
}

function check_link () {
    local script=$1
    local url=$2
    local true_hash=$3

    local out=$($script $url $OUT)
    local hash=$(md5 -q $out)
    rm $out

    if [ $hash = $true_hash ]; then
        return 0
    else
        return 1
    fi
}

function check_links () {
    local error=0
    local success=0

    while IFS=$'\t' read -r hash url; do
        for script in tests/*; do
            check_link $script $url $hash
            local res=$?

            if [ $res -ne 0 ]; then
                log_error $script $url
                error=$((error + 1))
            else
                log_success $script $url
                success=$((success + 1))
            fi
        done

    done < $LINKS

    echo "Succeeded: $success/$((success + error))."

    if [ $error -eq 0 ]; then
        return 0
    else
        return 1
    fi
}

function main () {
    echo "Date: $(date)"

    check_links
    local val=$?

    if [ $val -eq 0 ]; then
        echo "Test passed."
    else
        echo "Test failed."
    fi
}

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@" > log.txt