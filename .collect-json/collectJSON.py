#!/usr/bin/env python3

import argparse
import collections
import datetime
import json
import os
import sys

LOG_FILE = ".log.txt"
DEFAULT_DATA_DIR = "/projects/linqs/www/public/datasets"
DEFAULT_FILE = "/projects/linqs/www/public/.datasets.js"
DEFAULT_TARGET = "info.json"

def collect_json(home_dir, target):
    datasets = {}

    files = [os.path.join(home_dir, dir, target) for dir in os.listdir(home_dir)]
    files = list(filter(os.path.isfile, files))
    files = sorted(files)

    for f in files:
        try:
            with open(f) as file:
                data = json.load(file)
                datasets[data["key"]] = data
        except Exception as e:
            error_message = str(e) + " in " + f
            log_error(home_dir, error_message)

    log_success(home_dir)

    return datasets

def log_error(home_dir, e):
    m = str(datetime.datetime.now()) + ": " + e + "\n"
    with open(os.path.join(home_dir, LOG_FILE), "a") as f:
        f.write(m)

def log_success(home_dir):
    m = str(datetime.datetime.now()) + ": success" + "\n"
    with open(os.path.join(home_dir, LOG_FILE), "a") as f:
        f.write(m)

def main():
    parser = argparse.ArgumentParser(description='Collect JSON from each subfolder into one JSON array.')
    parser.add_argument("data_dir", type = str, nargs = "?", default = DEFAULT_DATA_DIR, help = "default data directory")
    parser.add_argument("write_file", type = str, nargs = "?", default = DEFAULT_FILE, help = "default file to write to")
    parser.add_argument("target_file", type = str, nargs = "?", default = DEFAULT_TARGET, help = "name of target file")

    args = parser.parse_args()

    data_dir = args.data_dir
    write_file = args.write_file
    target_file = args.target_file

    datasets = collect_json(data_dir, target_file)

    js = """\"use strict\";
window.linqs = window.linqs || {};
window.linqs.data = window.linqs.data || {};

window.linqs.data.datasets = """

    datasets_js = js + json.dumps(datasets, indent = 4, sort_keys = True, ensure_ascii = False) + ";"

    with open(write_file, 'w') as f:
        f.write(datasets_js)

if (__name__ == "__main__"):
    main()